<?php
// Name of the file
$filename = 'db_backup/my_wiki_dump.sql';
// MySQL host
$mysql_host = 'localhost';
// MySQL username
$mysql_username = 'root';
// MySQL password
$mysql_password = 'root';
// Database name
$mysql_database = 'my_wiki';
 
//////////////////////////////////////////////////////////////////////////////////////////////

set_time_limit(400);
// Connect to MySQL server
mysql_connect($mysql_host, $mysql_username, $mysql_password) or die('Error connecting to MySQL server: ' . mysql_error());

$sql = 'DROP DATABASE IF EXISTS '.$mysql_database;
$retval = mysql_query($sql) or die('Error dropping db : ' . mysql_error());
$sql = 'CREATE DATABASE IF NOT EXISTS ' . $mysql_database;
$retval = mysql_query($sql) or die('Error creating db : ' . mysql_error());; 

// Select database
mysql_select_db($mysql_database) or die('Error selecting MySQL database: ' . mysql_error());

// Temporary variable, used to store current query
$templine = '';
// Read in entire file
$lines = file($filename);
// Loop through each line
foreach ($lines as $line)
{
    // Skip it if it's a comment
    if (substr($line, 0, 2) == '--' || $line == '')
        continue;
 
    // Add this line to the current segment
    $templine .= $line;
    // If it has a semicolon at the end, it's the end of the query
    if (substr(trim($line), -1, 1) == ';')
    {
		if($templine != ';') {
        // Perform the query
        mysql_query($templine);
        // Reset temp variable to empty
		}
        $templine = '';
    }
}
 
?>
<script>
alert('db restore done');
</script>